
//for inheritance
// import Text from './inheritance/Text';

// function App() {
// 	return (
// 		<div className="App">
// 			<Text />
// 		</div>
// 	);
// }

//for composition
import Bracket from './composition/Bracket';
import Emoji from './composition/Emoji';
import Text from './composition/Text';

function App() {
	return (
		<Emoji>
			{/* {({addEmoji}) => <Text addEmoji={addEmoji} />} */}
			{({ addEmoji }) => (
				<Bracket>
					{({ addFirstBracket, addSecondBracket }) => <Text addEmoji={addEmoji} addFirstBracket={addFirstBracket} addSecondBracket={addSecondBracket} />}
				</Bracket>
			)}
		</Emoji>
	);
}

export default App;
