import Emoji from './Emoji';

//extends emoji for inheriting
class Text extends Emoji {
	//empty constructor/ optional
	// constructor(props) {
	// 	super(props);
	// }

	render() {
		//access parent method by this
		const text = this.addEmoji('i love Reactjs', 'emoji');
		//access parent render method by super because this(this.) will 
		//refer current render method
		return super.render(text);
	}
}

export default Text;
