import React from 'react';

export default class Emoji extends React.Component {
	addFirstBracket = (text) => `( ${text} )`;
	addSecondBracket = (text) => `{ ${text} }`;

	render() {
		return this.props.children({ addFirstBracket: this.addFirstBracket, addSecondBracket: this.addSecondBracket });
	}
}