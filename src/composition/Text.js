export default function Text({ addEmoji, addFirstBracket, addSecondBracket }) {
	let text = 'I am JavaScript Programming Language.';
	if (addEmoji) {
		text = addEmoji(text, '💜');
	}
	if (addFirstBracket) {
		text = addFirstBracket(text);
	}

	if (addSecondBracket) {
		text = addSecondBracket(text);
	}

	return <div>{text}</div>;
}